//
//  TabBar.swift
//  MovieDB
//
//  Created by Разработчик on 22.01.2022.
//

import UIKit

class TabBar: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        UITabBar.appearance().barTintColor = .systemBackground
        tabBar.tintColor = .label
        tabBars()
    }
    
    func tabBars() {
            viewControllers = [
                createNavController(for: childViewController(), title: NSLocalizedString("MoviesList", comment: ""), image: UIImage(systemName: "film")!),
                createNavController(for: ViewController(), title: NSLocalizedString("Favorites", comment: ""), image: UIImage(systemName: "star.fill")!),
            ]
        }
    
    fileprivate func createNavController(for rootViewController: UIViewController,
                                                title: String,
                                                image: UIImage) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        navController.navigationBar.prefersLargeTitles = true
        rootViewController.navigationItem.title = title
        return navController
        }
   

}
