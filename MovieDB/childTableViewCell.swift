//
//  childTableViewCell.swift
//  MovieDB
//
//  Created by Разработчик on 22.01.2022.
//

import UIKit

class childTableViewCell: UITableViewCell {
    
    @IBOutlet weak var moviesImageCell: UIImageView!
    @IBOutlet weak var moviesTittleCell: UILabel!
    @IBOutlet weak var moviesOverviewCell: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
